# Cara Build & Running


## Spesifikasi
- Java 11
- Maven Springboot
- PostgreSQL

## Sebelum Build & Run
Jangan lupa untuk Setting terlebih dahulu Database PostgreSQL pada "application.properties".
sesuaikan Database yang di create dengan settingan pada code.
```
src/main/resources/application.properties
```
```
spring.datasource.url= jdbc:postgresql://localhost:5432/devtest
spring.datasource.username= postgres
spring.datasource.password= postgres
```

## Run menggunakan Springboot
```
mvn spring-boot:run
```


## Masukkan pada DBMS PostgreSQL setelah berhasil Running

- INSERT INTO roles(name) VALUES('ROLE_USER');
- INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
- INSERT INTO roles(name) VALUES('ROLE_ADMIN');