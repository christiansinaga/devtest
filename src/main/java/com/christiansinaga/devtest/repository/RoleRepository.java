package com.christiansinaga.devtest.repository;

import com.christiansinaga.devtest.models.ERole;
import com.christiansinaga.devtest.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
