package com.christiansinaga.devtest.controllers;

import com.christiansinaga.devtest.models.User;
import com.christiansinaga.devtest.payload.response.UserResponse;
import com.christiansinaga.devtest.repository.UserRepository;
import com.christiansinaga.devtest.security.jwt.AuthTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1")
public class UserController {

    private final String USER = "hasRole('USER')";

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;


    @GetMapping("/profile")
    @PreAuthorize(USER)
    public UserResponse userAccess() {
        Optional<String> currentUserLogin = AuthTokenFilter.getCurrentUserLogin();
        Optional<User> currentUser = currentUserLogin.map(userRepository::findByUsername).orElse(null);
        UserResponse userResponse = new UserResponse();
        if (currentUser != null && currentUser.isPresent()) userResponse = new UserResponse(currentUser.get());
        return userResponse;
    }

    @DeleteMapping("/delete_user")
    @PreAuthorize(USER)
    public String deleteUser() {
        Optional<String> currentUserLogin = AuthTokenFilter.getCurrentUserLogin();
        Optional<User> currentUser = currentUserLogin.map(userRepository::findByUsername).orElse(null);
        if (currentUser != null && currentUser.isPresent()) {
            try {
                userRepository.deleteById(currentUser.get().getId());
                return "Success delete User";
            } catch (Exception e) {
                return "Failed to delete User";
            }
        }
        return "Failed to delete User";
    }

    @PutMapping("/update_profile")
    @PreAuthorize(USER)
    public UserResponse updateUser(@RequestParam(value = "email", required = false) String email,
                           @RequestParam(value = "phone", required = false) String phone,
                           @RequestParam(value = "name", required = false) String name) {
        Optional<String> currentUserLogin = AuthTokenFilter.getCurrentUserLogin();
        Optional<User> currentUser = currentUserLogin.map(userRepository::findByUsername).orElse(null);
        User user = Objects.requireNonNull(currentUser).orElse(null);
        if (user != null) {
            if (email != null) user.setEmail(email);
            if (name != null) user.setPhone(name);
            if (phone != null) user.setPhone(phone);
            userRepository.save(user);
        }
        return user != null ? new UserResponse(user) : new UserResponse();
    }

    @PutMapping("/change_password")
    @PreAuthorize(USER)
    public String changePassword(@RequestParam(value = "password") String password) {
        Optional<String> currentUserLogin = AuthTokenFilter.getCurrentUserLogin();
        Optional<User> currentUser = currentUserLogin.map(userRepository::findByUsername).orElse(null);
        Objects.requireNonNull(currentUser).ifPresent(user -> user.setPassword(encoder.encode(password)));
        return "Success change password";
    }
}
